# Maintainer:  xiota
# Co-maintainer: Pellegrino Prevete <pellegrinoprevete@gmail.com
# Contributor: Mark Wagie <mark dot wagie at proton dot me>
# Contributor: Shengyu Zhang <la@archlinuxcn.org>
# Contributor: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: David Garfias <jose.garfias@ingenieria.unam.edu>
# Contributor: Igor <f2404@yandex.ru>
# Contributor: Lubosz Sarnecki <lubosz@gmail.com>
# Contributor: Ionut Biru <ibiru@archlinux.org>

_pkg="vte"
_pkgname="${_pkg}4"
pkgname="${_pkgname}-git"
pkgver=0.73.0.r26.g092d8b8f
_libver=2.91
_girver=3.91
pkgrel=1
pkgdesc="Virtual Terminal Emulator widget (GTK4)"
arch=(
  x86_64
  i686
  pentium4
  aarch64
  armv7h)
license=('LGPL')
url="https://gitlab.gnome.org/GNOME/${_pkg}"
depends=(
  fribidi
  gnutls
  gtk4
  pcre2
  systemd
  "${_pkg}-common"
)
makedepends=(
  fribidi
  gi-docgen
  git
  gnutls
  gobject-introspection
  gperf
  gtk4
  meson
  pcre2
  systemd
  vala
)
options=(
  !emptydirs
  !lto)
provides=(
  "lib${_pkg}-${_libver}-gtk4.so=0-64"
  "${_pkgname}=${pkgver}"
)
conflicts=(
  "${_pkgname}"
)

source=(
  "${_pkg}::git+${url}"
)
sha256sums=(
  'SKIP'
)

pkgver() {
  cd "${srcdir}/${_pkg}"

  _version=$(
    grep -E "^\s+version:\s+'([0-9]+\.[0-9]+\.[0-9]+)',\$" meson.build \
      | sed -E "s@^\s+version:\s+'([0-9]+\.[0-9]+\.[0-9]+)',\$@\1@"
  )
  _commit=$(
    git log -S "${_version}" -1 --pretty=oneline | sed 's@\ .*$@@'
  )
  _revision=$(
    git rev-list --count "${_commit}..HEAD"
  )
  _hash=$(
    git rev-parse --short HEAD
  )

  echo "${_version}.r${_revision}.g${_hash}"
}

prepare() {
  cd "${_pkg}"
}

build() {
  local meson_options=(
    -D b_lto=false
    -D docs=false
    -D gtk3=false
    -D gtk4=true
  )
  arch-meson "${_pkg}" build "${meson_options[@]}"
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

_pick() {
  local p="$1" f d; shift
  for f; do
    d="${srcdir}/${p}/${f#$pkgdir/}"
    mkdir -p "$(dirname "${d}")"
    mv "$f" "$d" 
    rmdir -p --ignore-fail-on-non-empty "$(dirname "${f}")"
  done
}

package() {
  DESTDIR="$pkgdir" ninja install -C build
  meson install -C build --destdir "${pkgdir}"
  cd "${pkgdir}"
  _pick gtk4 "usr/bin/${_pkg}-${_libver}-gtk4"
  _pick gtk4 "usr/include/${_pkg}-${_libver}-gtk4"
  _pick gtk4 "usr/lib/lib${_pkg}-${_libver}-gtk4.so"*
  _pick gtk4 "usr/lib/pkgconfig/${_pkg}-${_libver}-gtk4.pc"
  _pick gtk4 "usr/lib/girepository-1.0/Vte-${_girver}.typelib"
  _pick gtk4 "usr/share/gir-1.0/Vte-${_girver}.gir"
  _pick gtk4 "usr/share/vala/vapi/${_pkg}-${_libver}-gtk4."{deps,vapi}

  rm -rf "${pkgdir}/"*
  cd "${srcdir}"
  cp -r gtk4/* "${pkgdir}"
}

# vim:set sw=2 sts=-1 et:
